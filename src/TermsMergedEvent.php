<?php

namespace Drupal\term_merge;

use Drupal\taxonomy\Entity\Term;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event used to notify subscribers that terms were merged.
 */
class TermsMergedEvent extends Event {

  /**
   * Array of terms. These terms are getting merged into $targetTerm.
   *
   * @var \Drupal\taxonomy\Entity\Term[]
   */
  protected $sourceTerms;

  /**
   * Target Term. All $sourceTerms are getting merged into this.
   *
   * @var \Drupal\taxonomy\Entity\Term
   */
  protected $targetTerm;

  /**
   * Constructor.
   *
   * @param array $sourceTerms
   *   Terms to merge.
   * @param \Drupal\taxonomy\Entity\Term $targetTerm
   *   Target Term.
   */
  public function __construct(array $sourceTerms, Term $targetTerm) {
    $this->sourceTerms = $sourceTerms;
    $this->targetTerm = $targetTerm;
  }

  /**
   * Retrieve the terms that are being merged into the target term.
   *
   * @return array
   *   An array of terms to merge.
   */
  public function getSourceTerms() {
    return $this->sourceTerms;
  }

  /**
   * Get the single target term.
   *
   * @return \Drupal\taxonomy\Entity\Term
   *   Single target term object.
   */
  public function getTargetTerm() {
    return $this->targetTerm;
  }

}
