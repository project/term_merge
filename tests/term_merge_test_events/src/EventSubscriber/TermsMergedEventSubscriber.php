<?php

namespace Drupal\term_merge_test_events\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\term_merge\TermsMergedEvent;
use Drupal\term_merge\TermMergeEventNames;

/**
 * This event subscriber prints a message to the screen when
 * some terms are merged.
 */
class TermsMergedEventSubscriber implements EventSubscriberInterface {

  /**
   * Code that is executed when the event is triggered.
   */
  public function onTermMerge(TermsMergedEvent $event) {
    \Drupal::messenger()->addMessage('The TermsMergedEvent was triggered.');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[TermMergeEventNames::TERMS_MERGED][] = ['onTermMerge'];
    return $events;
  }

}
